﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMovementsScript : MonoBehaviour
{
    public float speed = 15;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 input = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));
        Vector3 mouseInput = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
        gameObject.transform.Translate(input * speed * Time.deltaTime);
        gameObject.transform.Rotate(new Vector3(mouseInput.y, mouseInput.x, 0));
    }
}
