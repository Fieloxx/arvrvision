﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour
{
    public int life = 10;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void DecreaseHealth(int damage) {
        life -= damage;
        Debug.Log("Taking damage, " + life + " remaining");
        if (life <= 0) {
            SceneManager.LoadScene("Labyrinth");
        }
    }
}
