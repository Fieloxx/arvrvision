﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class PlayerController : MonoBehaviour
{
    private SteamVR_TrackedController _controller;
    
    public GameObject collidingObject;
    public GameObject objectInHand;
    
    private void OnEnable() {
        _controller = GetComponent<SteamVR_TrackedController>();
        if (_controller != null) {
            _controller.TriggerClicked += HandleTriggerClicked;
            _controller.PadClicked += HandlePadClicked;
            _controller.Gripped += HandleGripClicked;
        }
    }
    
    private void OnDisable()
    {
        if (_controller != null) {
            _controller.TriggerClicked -= HandleTriggerClicked;
            _controller.PadClicked -= HandlePadClicked;
            _controller.Gripped -= HandleGripClicked;
        }
    }
    
    private void HandleTriggerClicked(object sender, ClickedEventArgs e)
    {
        if (collidingObject != null && objectInHand == null) {
            PickupObject();
        } else if (objectInHand != null) {
            ReleaseObject();
        } else {
            
        }
    }

    private void HandleGripClicked(object sender, ClickedEventArgs e)
    {
        // Something to do ?
    }

    private void HandlePadClicked(object sender, ClickedEventArgs e)
    {
        // Obly for teleportation here
    }
    
    
    void OnTriggerEnter(Collider other)
    {
        if (!other.GetComponent<Rigidbody>())
        {
            return;
        }

        collidingObject = other.gameObject;
    }

    void OnTriggerExit(Collider other)
    {
        collidingObject = null;
    }

    

   private void PickupObject()
    {
        if (collidingObject.tag == "item") {
            print("Grabbing object !");
            objectInHand = collidingObject;
            objectInHand.transform.SetParent(this.transform);
            objectInHand.GetComponent<Rigidbody>().isKinematic = true;
            objectInHand.GetComponent<ItemBehaviour>().PickupToggle();
        }
    }

    private void ReleaseObject()
    {
        objectInHand.GetComponent<Rigidbody>().isKinematic = false;
        objectInHand.GetComponent<Rigidbody>().useGravity = false;
        objectInHand.transform.SetParent(null);
        objectInHand.GetComponent<ItemBehaviour>().PickupToggle();
        objectInHand = null;
    }
    
}
