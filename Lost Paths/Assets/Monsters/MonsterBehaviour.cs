﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MonsterBehaviour : MonoBehaviour
{
    public int detectionRange = 100;
    public int attackRange = 5;
    public int damage = 1;
    
    private NavMeshAgent _nav;
    public GameObject _player;
    
    Animator _animator;
    
    public int life = 3;
    private bool moving = false;
    private bool attacking = false;
    
    private float distance;

    public string idleAnimationName = "Idle";
    public string hitAnimationName = "Get Hit";

    private bool alive = true;
    
    void Start() {
        _nav = GetComponent<NavMeshAgent>();
        //_player = GameObject.FindGameObjectWithTag("Player");
        _animator = GetComponent<Animator>();
    }
    
    void Update() {
        if (alive)
        {
            distance = Vector3.Distance(transform.position, _player.transform.position);
            if (distance < detectionRange)
            {
                if (distance > attackRange && !attacking)
                {
                    if (!moving)
                    {
                        _nav.Resume();
                        moving = true;
                        _animator.Play("Run");
                    }
                    _nav.SetDestination(_player.transform.position);
                }
                else if (!attacking)
                {
                    Debug.Log("Distance is too small to move : " + distance + " attack range = " + attackRange);
                    _nav.Stop();
                    _nav.velocity = Vector3.zero;
                    moving = false;
                    _animator.Play("BasicAttack");
                    attacking = true;
                    // Attack();
                }
            }
            else
            {
                if (moving)
                {
                    _animator.Play(idleAnimationName);
                }
                moving = false;
                // Random move ?
            }
        }
    }
    
    private void Attack() {
        attacking = false;
        if (distance < attackRange) {
           _player.GetComponent<PlayerHealth>().DecreaseHealth(damage);
        } else {
            Debug.Log("Monster attack missed");
        }
    }

    public void DecreaseLife(int damage) {
        life -= damage;
        _animator.Play(hitAnimationName);
        attacking = false;
        if (life <= 0) {
            _animator.Play("Die");
            Die();
        }
    }
    
    private void Die() {
        alive = false;
        gameObject.tag = "Untagged";
    }
}
