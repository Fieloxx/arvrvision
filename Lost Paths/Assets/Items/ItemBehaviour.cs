﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBehaviour : MonoBehaviour
{
    public int damage = 1;
    private bool picked = false;
    
    void OnTriggerEnter(Collider other) {
        if (picked && other.gameObject.tag == "enemy") {      
            var mb = other.GetComponent<MonsterBehaviour>();
            mb.DecreaseLife(damage);
        }
    }

    public void PickupToggle()
    {
        picked = !picked;
    }
}
