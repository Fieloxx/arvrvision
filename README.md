# Lost Paths

Lost Paths est un labyrinthe en réalité virtuelle, dans lequel vous allez devoir combattre des dragons pour en sortir vivant.

### Informations complémentaires

Le build des lumières prenant plusieurs dizaines d'heures, l'exécutable n'est visuellement pas aussi joli que le jeu lancé depuis Unity. Afin d'apprécier l'aspect graphique du jeu, merci de tester le jeu depuis Unity. 
